from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls), name='admin'),
    url(r'', include('surveytool.urls')),
    url(r'', include('settings.urls')),
]