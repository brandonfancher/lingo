from __future__ import absolute_import

from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.generic.edit import DeleteView
from django_tables2 import RequestConfig

from .models import EmailTemplate
from .forms import EditEmailTemplateForm
from .tables import EmailTemplatesTable
from .filters import EmailTemplateFilter


def list_email_templates(request):
    template = 'surveytool/navigate/list_email_templates.html'

    queryset = EmailTemplate.objects.all()  # Get queryset with django-filter
    f = EmailTemplateFilter(request.GET, queryset=queryset)

    table = EmailTemplatesTable(f.qs)  # Pass queryset to django-tables2

    RequestConfig(request).configure(table)
    return render(request, template, {'table': table, 'filter': f})


def add_or_edit_email_template(request, template_no=None):
    template = 'surveytool/navigate/add_or_edit_email_template.html'
    email = EmailTemplate.objects.filter(pk=template_no).first()
    form = EditEmailTemplateForm(request.POST or None, instance=email)
    if form.is_valid():
        email_template = form.save()
        email_template.save()
        messages.success(request, 'Email template saved successfully.')
        return redirect('list_email_templates')
    return render(request, template, {'email': email, 'form': form})


# TODO: Prevent default or last EmailTemplate of given type from being deleted.
class EmailTemplateDeleteModal(DeleteView):
    model = EmailTemplate
    template_name = "surveytool/navigate/modals/emailtemplate_confirm_delete.html"
    success_message = "Email template successfully deleted."

    def delete(self, request, *args, **kwargs):
        # Send the success message
        messages.success(self.request, self.success_message)
        return super(EmailTemplateDeleteModal, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        url = self.request.META.get('HTTP_REFERER')
        return url
