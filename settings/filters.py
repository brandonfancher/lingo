from __future__ import absolute_import

from django import forms

import django_filters

from .models import EmailTemplate


class EmailTemplateFilter(django_filters.FilterSet):
    email_type = django_filters.MultipleChoiceFilter(choices=EmailTemplate.EMAIL_TYPE_CHOICES)
    name = django_filters.CharFilter(
        name="name",
        lookup_type='icontains',
        widget=forms.TextInput(attrs={'placeholder': 'Template Name'}))
    subject = django_filters.CharFilter(
        name="subject",
        lookup_type='icontains',
        widget=forms.TextInput(attrs={'placeholder': 'Subject Line'}))

    class Meta:
        model = EmailTemplate
        fields = ['email_type', 'name', 'subject', 'is_active', 'is_default']
