from __future__ import absolute_import

from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.core.urlresolvers import reverse

import django_tables2 as tables
from django_tables2.utils import A

from .models import EmailTemplate


class EmailTemplatesTable(tables.Table):
    actions = tables.Column(empty_values=(), orderable=False)

    def render_actions(self, record):
        id = A('id').resolve(record)
        edit_url = reverse('edit_email_template', args=[id])
        del_url = reverse('delete_email_template', args=[id])
        return mark_safe(
            '<a href="%s" style="color: black;">\
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;\
            <a data-toggle="modal" href="%s" data-target="#del-mkt-modal" style="color: black;">\
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>'
            % (escape(edit_url), escape(del_url)))

    class Meta:
        model = EmailTemplate
        attrs = {"class": "table table-striped"}
        template = "surveytool/table.html"
        fields = ('name', 'email_type', 'subject', 'is_active', 'is_default')
