from __future__ import absolute_import

from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(r'^navigate/settings/email-templates/edit-(?P<template_no>\d+)/$',
        views.add_or_edit_email_template, name='edit_email_template'),
    url(r'^navigate/settings/email-templates/add/$',
        views.add_or_edit_email_template, name='add_email_template'),
    url(r'^navigate/settings/email-templates/$',
        views.list_email_templates, name='list_email_templates'),
    url(r'^navigate/settings/email-templates/delete-email-template-(?P<pk>\d+)/$',
        views.EmailTemplateDeleteModal.as_view(), name='delete_email_template'),
    url(r'^navigate/settings/$',
        TemplateView.as_view(template_name="surveytool/navigate/settings.html"),
        name='app_settings'),
]
