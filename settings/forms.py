from __future__ import absolute_import

from django import forms

from .models import EmailTemplate


class EditEmailTemplateForm(forms.ModelForm):

    class Meta:
        model = EmailTemplate
        exclude = ()
        # labels = {
        #     'survey_name': 'Survey Name',
        #     'job_type': 'Project Type'
        # }

