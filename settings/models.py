from django.db import models


class EmailTemplate(models.Model):
    name = models.CharField(max_length=128)
    subject = models.CharField(max_length=128)
    body_html = models.TextField()
    body_text = models.TextField()
    is_active = models.BooleanField(default=False)
    is_default = models.BooleanField(default=False)

    GBE_ASSIGN = "GBE_ASG"
    GBE_REQUEST = "GBE_REQ"
    IF_ASSIGN = "IF_ASG"
    IF_REQUEST = "IF_REQ"
    SIW_ASSIGN = "SIW_ASG"
    SIW_REQUEST = "SIW_REQ"

    EMAIL_TYPE_CHOICES = (
        (GBE_ASSIGN, 'GBE Assignment'),
        (GBE_REQUEST, 'GBE Request'),
        (IF_ASSIGN, 'IF Assignment'),
        (IF_REQUEST, 'IF Request'),
        (SIW_ASSIGN, 'SIW Assignment'),
        (SIW_REQUEST, 'SIW Request'),
    )

    email_type = models.CharField(blank=False, choices=EMAIL_TYPE_CHOICES, max_length=10)

    def save(self, *args, **kwargs):
        """
        There should only be one default template per email_type. This overrides the model's
        save method to remove other defaults of the same email_type when saving a new
        template as default.
        """
        if self.is_default:
            EmailTemplate.objects.filter(
                email_type=self.email_type, is_default=True).update(is_default=False)
        super(EmailTemplate, self).save(*args, **kwargs)

    def __unicode__(self):
        return "%s: %s" % (self.get_email_type_display(), self.subject)
