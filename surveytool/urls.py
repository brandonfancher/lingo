from __future__ import absolute_import

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^jobs/$',
        views.view_jobs, name='view_jobs'),
    url(r'^jobs/survey-(?P<survey_no>\d+)/job-(?P<job_no>\d+)/$',
        views.take_survey, name='take_survey'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/$',
        views.view_survey, name='view_survey'),
    url(r'^navigate/create-survey/$',
        views.create_survey, name='create_survey'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/edit/$',
        views.edit_survey, name='edit_survey'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/edit/markets/$',
        views.list_surveymarkets, name='edit_survey_markets'),
    url(r'^navigate/surveys/$',
        views.list_surveys, name='list_surveys'),
    url(r'^navigate/delete-job-(?P<pk>\d+)/$',
        views.JobDeleteModal.as_view(), name='delete_job'),
    url(r'^navigate/delete-survey-(?P<pk>\d+)/$',
        views.SurveyDeleteModal.as_view(), name='delete_survey'),
    url(r'^navigate/delete-surveymkt-(?P<pk>\d+)/$',
        views.SurveyMarketDeleteModal.as_view(), name='delete_survey_market'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/market-(?P<market_no>\d+)/$',
        views.add_or_edit_surveymarket, name='edit_survey_market'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/add-market/$',
        views.add_or_edit_surveymarket, name='add_survey_market'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/job-(?P<job_no>\d+)/$',
        views.add_or_edit_job, name='edit_job_modal'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/add-job/$',
        views.add_or_edit_job, name='add_job'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/job-(?P<job_no>\d+)/edit/$',
        views.edit_job_detail, name='edit_job_detail'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/job-(?P<job_no>\d+)/detail/$',
        views.view_job_detail, name='view_job_detail'),
    url(r'^navigate/survey-(?P<survey_no>\d+)/job-(?P<job_no>\d+)/assign-(?P<linguist_no>\d+)/$',
        views.assign_linguist, name='assign_linguist'),
]
