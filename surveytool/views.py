from __future__ import absolute_import

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.forms.models import inlineformset_factory
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import DeleteView
from django_tables2 import RequestConfig

import sendgrid

from .models import Survey, SurveyMarket, Answer, Candidate, Job, User, UserProfile
from .forms import CreateSurveyForm, EditSurveyForm, AddSurveyMarketForm, \
    AddJobForm, action_formset
from .tables import SurveyViewTable, JobsViewTable, SurveyMarketsTable, QualJobLinguistsTable
from .filters import SurveyFilter, JobFilter

from settings.models import EmailTemplate


# TODO: Internal notification system. Q&A?
# https://youtu.be/EQLbkv8SgB4?list=PLxxA5z-8B2xk4szCgFmgonNcCboyNneMD

# Sendgrid API Key
sg_api_key = getattr(settings, "SENDGRID_API_KEY", None)


@login_required
def view_jobs(request):
    user = request.user
    jobs = Job.objects.filter(linguist=user.id)
    jobs_not_started = Job.objects.filter(linguist=user.id, status='NS')
    context_dict = {'jobs': jobs, 'unassigned_jobs': jobs_not_started}
    return render(request, 'surveytool/jobs.html', context_dict)


@login_required
def take_survey(request, survey_no, job_no):
    try:
        survey = Survey.objects.get(pk=survey_no)
        job = survey.jobs.get(pk=job_no)
        assigned_user = User.objects.get(user_profile__linguist_job=job)
        user = request.user

        if assigned_user == user:
            candidates = survey.candidates.all()
            questions = survey.questions.all()

            for c in candidates:
                for q in questions:
                    Answer.objects.get_or_create(
                        question=q,
                        job=job,
                        survey=survey,
                        candidate=c)

            queryset = Answer.objects.filter(job=job)

            answer_formset = inlineformset_factory(
                Candidate, Answer, fields=('answer',), max_num=0)
            formsets = []
            candidate_num = 0

            if request.method == 'POST':
                for c in candidates:
                    candidate_num += 1
                    c_id = 'candidate' + str(candidate_num)
                    formset = answer_formset(
                        request.POST,
                        instance=c,
                        queryset=queryset,
                        prefix=c_id)
                    formset.error_css_class = 'error'
                    formsets.append(formset)

                    if 'pend' in request.POST:
                        for form in formset:
                            field_prefix = form.add_prefix('answer')
                            field_value = form.fields['answer'].\
                                widget.value_from_datadict(
                                form.data, form.files, field_prefix)
                            if field_value.strip() == '':
                                updated_data = form.data.copy()
                                updated_data.__setitem__(
                                    field_prefix, "[unanswered]")
                                form.data = updated_data

                    if formset.is_valid():
                        formset.save(commit=True)
                    else:
                        print formset.errors

                if all(fset.is_valid() for fset in formsets) and 'pend' in request.POST:
                    job.status = Job.PENDED
                    job.save()
                    return redirect('/jobs/', permanent=True)
                elif all(fset.is_valid() for fset in formsets):
                    job.status = Job.SUBMITTED
                    job.save()
                    return redirect('/jobs/', permanent=True)  # See if we can get rid of this rep.

            else:
                job.status = Job.INPROGRESS

                for c in candidates:
                    for q in questions:
                        ans = Answer.objects.filter(
                            question=q,
                            job=job,
                            survey=survey,
                            candidate=c)[0]

                        if ans.answer == '[unanswered]':
                            ans.answer = ""
                            ans.save()

                    candidate_num += 1
                    c_id = 'candidate' + str(candidate_num)
                    formset = answer_formset(
                        instance=c,
                        queryset=queryset,
                        prefix=c_id)
                    formsets.append(formset)

                job.save()

            context_dict = {'survey': survey,
                            'formsets': formsets,
                            'candidate': candidates,
                            'job': job}

            return render(request, 'surveytool/take_survey.html', context_dict)
        else:
            return HttpResponse(
                "Fail IF: You are not logged in or do not have proper permissions!")

    except (Survey.DoesNotExist, Job.DoesNotExist, User.DoesNotExist):
        pass

    return HttpResponse(
        "EXCEPT: You are not logged in or do not have proper permissions!")


def create_jobs(survey, markets, num_lings_set):
    """
    Checks to see how many Jobs exist for that SurveyMarket.
    If fewer Jobs exist than is set on the SurveyMarket,
    it creates enough Jobs to equal the number of Jobs set.
    This will not delete or modify any existing Jobs.
    """
    for market in markets:
        num_lings_exist = len(Job.objects.filter(survey=survey, market=market))
        for i in range(0, (num_lings_set-num_lings_exist)):
            job = Job(survey=survey, market=market)
            job.save()


@login_required
def create_survey(request):

    template = 'surveytool/navigate/create_survey.html'

    # sticks in a POST or renders empty form
    form = CreateSurveyForm(request.POST or None)

    if form.is_valid():
        survey = form.save(commit=False)
        survey.save()
        for question in form.cleaned_data['questions']:
            survey.questions.add(question)
        num_linguists = form.cleaned_data['num_linguists']
        markets = form.cleaned_data['markets']
        for market in markets:
            mkt = SurveyMarket(survey=survey, market=market, num_linguists=num_linguists)
            mkt.save()

        create_jobs(survey, markets, num_linguists)

        candidates_txt = form.cleaned_data['candidates_txt']
        for candidate in candidates_txt.splitlines():
            candidate = Candidate(candidate=candidate, survey=survey)
            candidate.save()

        messages.success(request, '"%s" successfully created.' % survey.name)

        return redirect('view_survey', survey_no=survey.id)

    else:
        print form.errors

    return render(request, template, {'form': form})


# TODO: Need to introduce name ordering
# TODO: Need to add ability to edit Names
# TODO: This should prob go elsewhere, but what about names with superscript and other styling?
def edit_survey(request, survey_no):
    template = 'surveytool/navigate/edit_survey.html'
    survey = get_object_or_404(Survey, pk=survey_no)
    form = EditSurveyForm(request.POST or None, instance=survey)
    if form.is_valid():
        survey = form.save()
        survey.save()
        messages.success(request, 'Changes made successfully.')
        return redirect('view_survey', survey_no=survey.id)
    return render(request, template, {'form': form, 'survey': survey})


def list_surveymarkets(request, survey_no):
    template = "surveytool/navigate/list_surveymarkets.html"
    survey = get_object_or_404(Survey, pk=survey_no)
    table = SurveyMarketsTable(SurveyMarket.objects.filter(survey=survey))
    RequestConfig(request).configure(table)
    return render(request, template, {'table': table, 'survey': survey})


# TODO: Catch error if SurveyMarket exists, or exclude existing markets from form market field
def add_or_edit_surveymarket(request, survey_no, market_no=None):
    """
    From the edit_survey_markets page, this enables the New Market and Edit buttons and modals.
    """
    template = 'surveytool/navigate/modals/add_or_edit_surveymarket.html'
    survey = get_object_or_404(Survey, pk=survey_no)
    survey_market = SurveyMarket.objects.filter(pk=market_no).first()  # returns first obj or None.
    form = AddSurveyMarketForm(request.POST or None, instance=survey_market)
    if form.is_valid():
        new_mkt = form.save(commit=False)
        new_mkt.survey = survey
        new_mkt.save()

        # Create Jobs
        market = []
        market.append(new_mkt.market)
        create_jobs(survey, market, new_mkt.num_linguists)

        messages.success(request, 'Market added successfully.')
        return redirect('edit_survey_markets', survey_no=survey_no)
    return render(
        request, template, {'survey': survey, 'form': form, 'market_no': market_no})


def add_or_edit_job(request, survey_no, job_no=None):
    """
    From the view_survey page, this enables the New Job and Edit buttons and modals.

    TODO: Editing a job shouldn't be a modal. It should take you to a Job detail page
    with a related list of linguists that meet the job criteria, their information,
    and their price for that specific Job. Eventually, it should also have an
    Availability Request history chart. To assign a linguist to the Job, click the
    Assign action button next to that linguist. Then that can bring up a modal for
    sending the email. Or select several and choose the "Send ARs" Item Action option.
    """
    template = 'surveytool/navigate/modals/add_or_edit_job.html'
    survey = get_object_or_404(Survey, pk=survey_no)
    job = Job.objects.filter(pk=job_no).first()  # first() returns first obj or None.
    form = AddJobForm(request.POST or None, instance=job)
    initial_linguist = form.initial.get('linguist')  # Gets ID or None
    if form.is_valid():
        new_job = form.save(commit=False)
        new_job.survey = survey

        # Assign appropriate Job status based on linguist change.
        new_linguist = new_job.linguist  # Gets linguist or None
        if new_linguist is None:
            new_job.status = Job.UNASSIGNED
        elif new_linguist.id is not initial_linguist:
            new_job.status = Job.NOTSTARTED

        new_job.save()
        messages.success(request, 'Job modified or added successfully.')
        return redirect('view_survey', survey_no=survey_no)
    return render(
        request, template, {'survey': survey, 'form': form, 'job_no': job_no})


def edit_job_detail(request, survey_no, job_no):
    template = 'surveytool/navigate/edit_job.html'
    job = Job.objects.filter(pk=job_no).first()  # first() returns first obj or None.
    form = AddJobForm(request.POST or None, instance=job)
    if form.is_valid():
        job = form.save()
        job.save()
        messages.success(request, 'Job saved successfully.')
    return render(request, template, {'job': job, 'form': form})


# TODO: Build out linguist table to display related job rate information.
# May have to pass it more context (number of names, the market, etc.)
# Maybe figure out a way to show request status on this table too?
# I'm not sure if that's possible or even beneficial, yet.
def view_job_detail(request, survey_no, job_no):
    template = 'surveytool/navigate/view_job.html'
    job = get_object_or_404(Job, pk=job_no)

    # Filter the queryset ...
    qualified_linguists = UserProfile.objects.filter(rate_profile__market=job.market)
    f = JobFilter(request.GET, queryset=qualified_linguists)

    # ... and pass it to django-tables2, along with Job context.
    table = QualJobLinguistsTable(f.qs, job=job)
    RequestConfig(request, paginate={"per_page": 20}).configure(table)

    return render(request, template, {'job': job, 'table': table})


# This will prob need to reference VendorRateProfile
# TODO: Redirect to page or modal where email template can be changed and modified, WYSIWYG.
# TODO: Build area for creating and modifying email templates.
# TODO: Restrict this view to only assigning linguists to UNASSIGNED jobs.
# Jobs already assigned should be canceled or disposed of since they contain answers.
# Or clear all answers upon reassign.
def assign_linguist(request, survey_no, job_no, linguist_no):
    job = get_object_or_404(Job, pk=job_no)
    linguist = get_object_or_404(UserProfile, pk=linguist_no)

    # TODO: Allow for other survey types
    if job.survey.job_type.abbreviation == 'GBE':
        email_type = 'GBE_ASG'

    # TODO: Ummm, I might want to consider actually *assigning* the linguist.

    default_email_template = EmailTemplate.objects.get(email_type=email_type, is_default=True)

    # TODO: Maybe turn this into a function?
    # TODO: Check status to ensure success and throw error if problem.
    # https://github.com/sendgrid/sendgrid-python#error-handling
    sg = sendgrid.SendGridClient(sg_api_key)
    message = sendgrid.Mail()
    message.set_from('brandon.f@travelingbrand.com')
    message.set_subject(default_email_template.subject)
    message.set_html(default_email_template.body_html)
    message.smtpapi.add_to(linguist.user.email)
    message.smtpapi.set_substitutions({'{{job_no}}': [job_no],
                                       '{{linguist_name}}': [linguist.user.first_name],
                                       '{{job_type}}': [job.survey.job_type.name]})
    status, msg = sg.send(message)

    # TODO: Return a response!


# The start of my attempt to make list actions in a modular way. We'll see.
def do_action(request, action, cls, pks):
    if action == 'Delete':
        for pk in pks:
            cls.objects.filter(pk=pk).delete()
        messages.success(request, 'Items deleted successfully.')


@login_required
def view_survey(request, survey_no):
    template = "surveytool/navigate/view_survey.html"
    survey = Survey.objects.get(pk=survey_no)

    names_list = ', '.join([name.candidate for name in survey.candidates.all()])
    markets = survey.markets.all().order_by('market')

    queryset = Job.objects.filter(survey=survey)  # Get queryset with django-filter
    f = JobFilter(request.GET, queryset=queryset)

    table = JobsViewTable(f.qs)  # Pass queryset to django-tables2
    RequestConfig(request, paginate={"per_page": 15}).configure(table)

    # Define list actions and process in do_action helper function.
    actions = ('Item Actions', 'Delete')
    action_formclass = action_formset(actions)
    if request.method == 'POST':
        chosen_action = request.POST['action']
        chosen_qset = request.POST.getlist("selection")
        do_action(request, chosen_action, Job, chosen_qset)

    action_form = action_formclass()

    return render(
        request,
        template,
        {'survey': survey,
         'names_list': names_list,
         'markets': markets,
         'table': table,
         'filter': f,
         'action_form': action_form})


# TODO: Make sure these can only work if logged in as superuser.
class JobDeleteModal(DeleteView):
    model = Job
    template_name = "surveytool/navigate/modals/job_confirm_delete.html"
    success_message = "Job successfully deleted."

    # While other generic class-based views don't require the following, DeleteView does.
    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(JobDeleteModal, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        url = self.request.META.get('HTTP_REFERER')
        return url


class SurveyDeleteModal(DeleteView):
    model = Survey
    template_name = "surveytool/navigate/modals/survey_confirm_delete.html"
    success_message = "Survey and all related Jobs successfully deleted."

    # While other generic class-based views don't require the following
    # for displaying a success message, DeleteView does.
    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(SurveyDeleteModal, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        url = self.request.META.get('HTTP_REFERER')
        return url


class SurveyMarketDeleteModal(DeleteView):
    model = SurveyMarket
    template_name = "surveytool/navigate/modals/surveymarket_confirm_delete.html"
    success_message = "Market successfully removed from this Survey."

    def delete(self, request, *args, **kwargs):
        # Also delete Jobs for this SurveyMarket
        obj = self.get_object()
        survey_market_jobs = Job.objects.filter(market=obj.market)
        survey_market_jobs.delete()
        # Send the success message
        messages.success(self.request, self.success_message)
        return super(SurveyMarketDeleteModal, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        url = self.request.META.get('HTTP_REFERER')
        return url


# TODO: Click on a Survey to go to a view page...or on a pencil and go to an edit page.
# TODO: Surveys need statuses
# TODO: Restrict to superuser
# TODO: Add Actions (with checkboxes) and delete buttons
@login_required
def list_surveys(request):
    template = "surveytool/navigate/list_surveys.html"
    # Get queryset with django-filter
    queryset = Survey.objects.all()
    f = SurveyFilter(request.GET, queryset=queryset)

    # Pass queryset to django-tables2
    table = SurveyViewTable(f.qs)

    RequestConfig(request, paginate={"per_page": 15}).configure(table)
    return render(request, template, {"surveys": table, "filter": f})
