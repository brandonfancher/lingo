from __future__ import absolute_import

from django import forms

from .models import Answer, Survey, Candidate, Job, UserProfile, SurveyMarket


# TODO: Integrate Widgets from https://select2.github.io or even Bootstrap widgets


class AnswerForm(forms.ModelForm):
    answer = forms.CharField(
        max_length=512, help_text="Your Answer: ", widget=forms.Textarea)

    def clean(self):
        data = self.cleaned_data['answer']
        if data is None:
            print "empty!"
        else:
            print "full"

        return data

    class Meta:
        model = Answer
        fields = ('answer',)


# This survey form may go away in favor of Django admin.
# Leaving here as example for now.
class CreateSurveyForm(forms.ModelForm):
    num_linguists = forms.IntegerField(label='Number of Linguists per Market',
        help_text='This can be customized on a market-by-market basis later.',
        required=True,
        max_value=9)
    candidates_txt = forms.CharField(max_length=20000,
                                     help_text="Paste names here, one per line.",
                                     label="Bulk Add Names",
                                     widget=forms.Textarea)

    # def __init__(self, *args, **kwargs):
    #     super(CreateSurveyForm, self).__init__(*args, **kwargs)
    #     self.fields['questions'].initial = [q.pk for q in Question.object.filter()]

    class Meta:
        model = Survey
        exclude = ()
        labels = {
            'survey_name': 'Survey Name',
            'job_type': 'Project Type'
        }


class EditSurveyForm(forms.ModelForm):

    class Meta:
        model = Survey
        exclude = ('markets',)
        labels = {
            'survey_name': 'Survey Name',
            'job_type': 'Project Type'
        }


class AddSurveyMarketForm(forms.ModelForm):

    class Meta:
        model = SurveyMarket
        exclude = ('survey',)
        labels = {
            'num_linguists': 'Number of Linguists'
        }


class AddJobForm(forms.ModelForm):

    class Meta:
        model = Job
        exclude = ('survey',)

    # Filter linguists by market on jobs you're changing so  form only shows relevant linguists.
    def __init__(self, *args, **kwargs):
        super(AddJobForm, self).__init__(*args, **kwargs)
        if self.instance.pk:  # Adding blank Job? Then don't apply this filter.
            self.fields['linguist'].queryset = \
                UserProfile.objects.filter(rate_profile__market=self.instance.market)


# Unnecessary Django Admin Stuff - Delete
class CreateSurveyFormAdmin(forms.ModelForm):
    candidates_txt = forms.CharField(
        max_length=20000,
        help_text="To add names, paste them here. Each name should be on a separate line.",
        widget=forms.Textarea,
        required=False, label="Add Names")

    def save(self, commit=True):

        candidates_txt = self.cleaned_data.get('candidates_txt', None)
        for candidate in candidates_txt.splitlines():
            candidate = Candidate(candidate=candidate, survey=self.instance)
            candidate.save()

        return super(CreateSurveyFormAdmin, self).save(commit=commit)

    class Meta:
        exclude = ()


# Unnecessary Django Admin Stuff - Delete
class SurveyJobChangeListForm(forms.ModelForm):
    """
    This model creates a new column on the Job changelist for linguists.
    It then sets it so that it only shows linguists in the picklist which
    work in the Job's language.
    """
    class Meta:
        model = Job
        exclude = ()

    linguist = forms.ModelChoiceField(queryset=UserProfile.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(SurveyJobChangeListForm, self).__init__(*args, **kwargs)
        self.fields['linguist'].queryset = \
            UserProfile.objects.filter(rate_profile__market=self.instance.market)


def action_formset(actions):
    """A form factory which returns a form which allows the user to pick a specific action to
    perform on a chosen subset of items from a queryset.
    """
    class _ActionForm(forms.Form):
        action = forms.ChoiceField(choices=zip(actions, actions), label='')

    return _ActionForm
