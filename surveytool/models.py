from django.db import models
from django.contrib.auth.models import User

# Use FormWizard? http://blog.stanisla.us/2009/10/28/using-a-formwizard-in-the-django-admin/
# https://www.youtube.com/watch?v=fSnBF-BmccQ (FormWizard AND SendMail!)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="user_profile")

    def __unicode__(self):
        return self.user.username


class VendorRateProfile(models.Model):
    user = models.ForeignKey('UserProfile', related_name="rate_profile")
    market = models.ForeignKey("Market")
    job_type = models.ForeignKey("ProjectType")
    min_fee = models.DecimalField(max_digits=8, decimal_places=2)
    name_limit = models.IntegerField()
    extra_name_fee = models.DecimalField(max_digits=4, decimal_places=2)

    class Meta:
        unique_together = ('user', 'market', 'job_type')

    def __unicode__(self):
        return "Vendor Rate Profile for %s" % self.user


class ProjectType(models.Model):
    name = models.CharField(max_length=128, verbose_name='Project Type', unique=True)
    abbreviation = models.CharField(max_length=128, verbose_name='Project Type', unique=True)

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.abbreviation)


class Survey(models.Model):
    name = models.CharField(max_length=128, verbose_name="Survey Name")
    customer = models.ForeignKey('Customer', verbose_name="Client")
    job_type = models.ForeignKey('ProjectType', verbose_name="Project Type")
    questions = models.ManyToManyField('Question')
    markets = models.ManyToManyField('Market', through='SurveyMarket')

    def __unicode__(self):
        return self.name


class Market(models.Model):
    market = models.CharField(max_length=128)
    language = models.CharField(max_length=128)

    def __unicode__(self):
        return '%s (%s)' % (self.market, self.language)


class SurveyMarket(models.Model):
    survey = models.ForeignKey('Survey')
    market = models.ForeignKey('Market')
    num_linguists = models.PositiveSmallIntegerField(verbose_name="# Linguists")

    class Meta:
        unique_together = ('survey', 'market')


class Customer(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.name


class Job(models.Model):
    linguist = models.ForeignKey('UserProfile', blank=True, null=True, related_name="linguist_job")
    survey = models.ForeignKey('Survey', related_name="jobs")
    market = models.ForeignKey('Market')

    # Ideas for future statuses:
    # Requested [Available, Rejected, Expired]
    # Open [Open, Incomplete, Submitted, Overdue]
    # Closed [Completed, Missed Deadline]
    UNASSIGNED = "UA"
    NOTSTARTED = 'NS'
    INPROGRESS = 'IP'
    PENDED = 'PD'
    SUBMITTED = "SB"
    STATUS_CHOICES = (
        (UNASSIGNED, 'Unassigned'),
        (NOTSTARTED, 'Not Started'),
        (INPROGRESS, 'In Progress'),
        (PENDED, 'Pended'),
        (SUBMITTED, 'Submitted'),
    )

    status = models.CharField(
        max_length=2, blank=False, choices=STATUS_CHOICES, default=UNASSIGNED)

    def __unicode__(self):
        return '%s - %s (%s/%s)' % (
            self.linguist, self.survey.name, self.survey.pk, self.pk)


class Question(models.Model):
    name = models.CharField(max_length=128, unique=True)
    question = models.TextField()

    def __unicode__(self):
        return self.name


class Candidate(models.Model):
    candidate = models.CharField(max_length=128)
    survey = models.ForeignKey(Survey, related_name='candidates')

    def __unicode__(self):
        return self.candidate


class Answer(models.Model):
    question = models.ForeignKey(Question)
    job = models.ForeignKey(Job)
    survey = models.ForeignKey(Survey)
    candidate = models.ForeignKey(Candidate)
    answer = models.TextField()

    def __unicode__(self):
        return "Answer - '%s' - '%s' - Name: '%s'" % (
            self.job, self.question, unicode(self.candidate))
