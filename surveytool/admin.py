from django.contrib import admin
from django.core.urlresolvers import reverse

import surveytool.models as m
from surveytool.forms import CreateSurveyFormAdmin, SurveyJobChangeListForm

# TODO: Add Django WP Admin (http://django-wp-admin.readthedocs.org/en/master/index.html)


class CandidatesInline(admin.TabularInline):
    model = m.Candidate
    extra = 0


class SurveyMarketsInline(admin.TabularInline):
    model = m.SurveyMarket
    extra = 30

    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            # Don't add any extra forms if the related object already exists.
            return 0
        return self.extra


class VendorRatesInline(admin.TabularInline):
    model = m.VendorRateProfile
    extra = 3


@admin.register(m.UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    inlines = [VendorRatesInline]


@admin.register(m.Job)
class JobAdmin(admin.ModelAdmin):

    def get_changelist_form(self, request, **kwargs):
        return SurveyJobChangeListForm

    def save_model(self, request, obj, form, change):
        """
        When the object is modified, set Job status accordingly.
        """
        old_linguist = form.initial.get('linguist')  # Gets ID or None

        new_linguist = obj.linguist  # Gets linguist or None
        if new_linguist:  # This must be done to avoid Id on NoneType error
            new_linguist = new_linguist.id

        if new_linguist is None:
            obj.status = obj.UNASSIGNED
        elif new_linguist is not old_linguist:
            obj.status = obj.NOTSTARTED

        obj.save()

    fields = ('id', 'survey', 'market', 'linguist', 'status')
    readonly_fields = ('id',)
    list_display = ('survey', 'id', 'market', 'linguist', 'status')
    list_editable = ('linguist',)

    # def changelist_view(self, request, extra_context=None):
    #    print "hi"
    #    self.list_editable = ('market',)
    #    return super(JobAdmin, self).changelist_view(request, extra_context=extra_context)


@admin.register(m.Survey)
class SurveyAdmin(admin.ModelAdmin):
    form = CreateSurveyFormAdmin

    readonly_fields = ('id', 'list_jobs')
    fields = ('id', 'name', 'customer', 'job_type', 'questions', 'candidates_txt', 'list_jobs')
    inlines = [CandidatesInline, SurveyMarketsInline]

    # TODO: This method needs to handle cases where all jobs are deleted
    # and it redirects upon save and there are no jobs to present.
    def list_jobs(self, obj):
        """
        This creates a "List Jobs" link to a change list showing only those jobs
        related to the current survey. If no jobs exist, it tells you that.
        """
        if m.Job.objects.filter(survey=obj):
            url = reverse('admin:surveytool_job_changelist')
            return '<a href="{0}?survey__id__exact={1}">List Jobs</a>'.format(url, obj.id)
        else:
            return 'No jobs have been created yet.'

    list_jobs.allow_tags = True
    list_jobs.short_description = "Assigned Jobs"

    def save_related(self, request, form, formsets, change):
        """
        This does two things:
        1) It first saves the inline formset Survey Markets objects to the database
        so that the number of linguists for each market is updated, and
        2) based on those saved values, it ensures there are at least that many jobs
        in existence for that market on that survey.
        """
        super(SurveyAdmin, self).save_related(request, form, formsets, change)

        surveymarkets = form.instance.surveymarket_set.all()
        for surveymarket in surveymarkets:
            num_lings_set = surveymarket.num_linguists
            num_lings_exist = \
                len(m.Job.objects.filter(survey=surveymarket.survey, market=surveymarket.market))
            for i in range(0, (num_lings_set-num_lings_exist)):
                job = m.Job(survey=surveymarket.survey, market=surveymarket.market)
                job.save()


@admin.register(m.VendorRateProfile)
class VendorRateProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'market', 'job_type', 'min_fee', 'name_limit', 'extra_name_fee')


admin.site.register(m.Question)
admin.site.register(m.Answer)
admin.site.register(m.Candidate)
admin.site.register(m.Customer)
admin.site.register(m.Market)
admin.site.register(m.ProjectType)
admin.site.register(m.SurveyMarket)
