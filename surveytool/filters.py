from __future__ import absolute_import

from django import forms

import django_filters

from .models import Survey, Job


class SurveyFilter(django_filters.FilterSet):
    """
    Using django-filter, this enables filtering of
    Survey objects using URL query strings.
    """
    name = django_filters.CharFilter(
        name="name",
        lookup_type='icontains',
        widget=forms.TextInput(attrs={'placeholder': 'Survey Name'}))
    customer = django_filters.CharFilter(
        name="customer__name",
        lookup_type='icontains',
        widget=forms.TextInput(attrs={'placeholder': 'Customer'}))
    type = django_filters.CharFilter(
        name="job_type__name",
        lookup_type='icontains',
        widget=forms.TextInput(attrs={'placeholder': 'Project Type'}))

    class Meta:
        model = Survey
        fields = ['name', 'customer', 'type']
        order_by = ['id',
                    '-id',
                    'name',
                    '-name',
                    'customer',
                    '-customer',
                    'job_type',
                    '-job_type'
                    ]


class JobFilter(django_filters.FilterSet):
    """
    Using django-filter, this enables filtering of
    Job objects using URL query strings.
    """
    status = django_filters.MultipleChoiceFilter(choices=Job.STATUS_CHOICES)

    class Meta:
        model = Job
        fields = ['linguist', 'market', 'status']
