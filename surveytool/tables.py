from __future__ import absolute_import

from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.core.urlresolvers import reverse

import django_tables2 as tables
from django_tables2.utils import A

from .models import Survey, Job, SurveyMarket, UserProfile


class SurveyViewTable(tables.Table):

    name = tables.LinkColumn('view_survey', args=[A('pk')])
    actions = tables.Column(empty_values=(), orderable=False)

    def render_actions(self, record):
        id = A('id').resolve(record)
        url = reverse('delete_survey', args=[id])
        return mark_safe(
            '<a data-toggle="modal" href="%s" data-target="#del-survey-modal" \
            style="color: black;"><span class="glyphicon glyphicon-trash" aria-hidden="true"> \
            </span></a>'
            % escape(url))

    class Meta:
        model = Survey
        attrs = {"class": "table table-striped"}
        template = "surveytool/table.html"
        fields = ('id', 'name', 'customer', 'job_type.name')


class SurveyMarketsTable(tables.Table):
    actions = tables.Column(empty_values=(), orderable=False)

    def render_actions(self, record):
        survey = A('survey').resolve(record)
        id = A('id').resolve(record)
        edit_url = reverse('edit_survey_market', args=[survey.pk, id])
        del_url = reverse('delete_survey_market', args=[id])
        return mark_safe(
            '<a data-toggle="modal" href="%s" data-target="#edit-mkt-modal" style="color: black;">\
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;\
            <a data-toggle="modal" href="%s" data-target="#del-mkt-modal" style="color: black;">\
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>'
            % (escape(edit_url), escape(del_url)))

    class Meta:
        model = SurveyMarket
        attrs = {"class": "table table-striped"}
        template = "surveytool/table.html"
        fields = ('market', 'num_linguists', 'delete')


class QualJobLinguistsTable(tables.Table):

    # Init with extra context data for actions links.
    def __init__(self, *args, **kwargs):
        self.job = kwargs.pop('job')
        super(QualJobLinguistsTable, self).__init__(*args, **kwargs)

    actions = tables.Column(empty_values=(), orderable=False)

    def render_actions(self, record):
        id = A('id').resolve(record)
        assign_url = reverse('assign_linguist', args=[self.job.survey.pk, self.job.pk, id])
        return mark_safe(
            '<a data-toggle="modal" href="%s" data-target="#assign-ling-modal"\
            style="color: black;"><span class="glyphicon glyphicon-plus" aria-hidden="true">\
            </span></a>'
            % assign_url)

    class Meta:
        model = UserProfile
        attrs = {"class": "table table-striped"}
        template = "surveytool/table.html"
        # fields = ('market', 'num_linguists', 'delete')


class JobsViewTable(tables.Table):
    id = tables.LinkColumn('view_job_detail', args=[A('survey.pk'), A('pk')])
    selection = tables.CheckBoxColumn(verbose_name="select",
                                      accessor='pk',
                                      attrs={"th__input": {"onclick": "toggle(this)"}},
                                      orderable=False)
    market = tables.Column(order_by='market.market')
    actions = tables.Column(empty_values=(), orderable=False)

    # hidden field for applying row style in conjuction with render_tr_class
    tr_class = tables.Column(visible=False, empty_values=())

    def render_tr_class(self, bound_row):
        status = bound_row['status']
        colors = {'Unassigned': 'active',
                  'Not Started': '',
                  'In Progress': '',
                  'Pended': 'info',
                  'Submitted': 'success'}
        return colors[status]

    def render_actions(self, record):
        survey = A('survey').resolve(record)
        id = A('id').resolve(record)
        edit_url = reverse('edit_job_modal', args=[survey.pk, id])
        del_url = reverse('delete_job', args=[id])
        return mark_safe(
            '<a data-toggle="modal" href="%s" data-target="#edit-job-modal" style="color: black;">\
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;\
            <a data-toggle="modal" href="%s" data-target="#del-job-modal" style="color: black;">\
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>'
            % (escape(edit_url), escape(del_url)))

    class Meta:
        model = Job
        attrs = {"class": "table table-striped"}
        template = "surveytool/table.html"
        fields = ('selection', 'id', 'linguist', 'market', 'status', 'actions')
